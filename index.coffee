module.exports = ( options ) ->

  @add 'role:transaction,action:success', ( msg, respond ) ->
    { __steps = [] } = msg

    return respond msg if __steps.length is 0

    @act 'role:transaction,action:serial', msg, respond

  @add 'role:transaction,action:error', ( msg, respond ) ->
    @act 'role:util,action:log', msg, respond

  @add 'role:transaction,action:forward', ( msg, respond ) ->
    respond null

  @add 'role:transaction,action:serial', ( msg, respond ) ->
    { __steps = [] } = msg
    step = __steps.shift()

    if step
      @act step.__action, {
        ...msg
        ...step
        __steps
      }, respond
    else
      console.error 'role:transaction,action:serial: unhandle empty steps'
      respond null
